package net.therap.annotatedvalidator;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/7/18
 */
public class AnnotatedValidator<T> {

    private T object;
    private List<ValidationError> errors;

    public AnnotatedValidator(T object) {
        this.object = object;
        this.errors = new ArrayList<ValidationError>();
    }

    public List<ValidationError> validate() {

        Field[] fields = object.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);

            int value;
            try {
                value = getFieldValue(field);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                continue;
            }

            Annotation[] annotations = field.getDeclaredAnnotations();

            for (Annotation annotation : annotations) {

                if (annotation instanceof Size) {
                    Size size = (Size) annotation;

                    if (value >= size.min() && value <= size.max()) {
                        continue;
                    } else {
                        ValidationError error = new ValidationError(field.getName());
                        makeValidationError(size, error);
                    }
                }
            }
        }
        return errors;
    }

    public void print(List<ValidationError> allErrors) {
        for (ValidationError error : allErrors) {
            System.out.println(error);
        }
    }

    private int getFieldValue(Field field) throws IllegalAccessException {
        return field.get(object).hashCode();
    }

    private void makeValidationError(Size size, ValidationError error) {

        String message = size.message();
        message = message.replace("{min}", String.valueOf(size.min()));
        message = message.replace("{max}", String.valueOf(size.max()));
        error.setMessage(message);

        errors.add(error);
    }
}
