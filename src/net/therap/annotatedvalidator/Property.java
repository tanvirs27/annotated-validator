package net.therap.annotatedvalidator;

import java.util.List;

/**
 * @author shahriar
 * @since 2/7/18
 */
public class Property {

    private int hash;

    public Property(String value) {
        hash = value.length();
    }

    public Property(Number value) {
        hash = value.intValue();
    }

    public <T> Property(List<T> list) {
        hash = list.size();
    }

    public int hashCode() {
        return hash;
    }
}
