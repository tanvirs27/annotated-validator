package net.therap.annotatedvalidator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/7/18
 */
public class Main {

    public static void main(String[] args) {

        Person person = new Person();
        person.setName("Rif");
        person.setAge(22);
        person.setAddress("mirpur, dhaka");
        person.setDebt(-1000.20);
        person.setFavBook("Da Vinci Code");

        List<String> friends = new ArrayList<String>();
        friends.add("jhashjsd");
        friends.add("asjk");
        person.setFriends(friends);

        AnnotatedValidator<Person> validator = new AnnotatedValidator<Person>(person);
        List<ValidationError> errors = validator.validate();
        validator.print(errors);
    }
}
