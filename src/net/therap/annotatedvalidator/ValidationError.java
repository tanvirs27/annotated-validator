package net.therap.annotatedvalidator;

/**
 * @author shahriar
 * @since 2/7/18
 */
public class ValidationError {

    private String variableName;
    private String message;

    public ValidationError(String variableName) {
        this.variableName = variableName;
        this.message = "";
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String toString() {
        return variableName + ": " + message;
    }
}
