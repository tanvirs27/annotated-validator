package net.therap.annotatedvalidator;

import java.util.List;

/**
 * @author shahriar
 * @since 2/7/18
 */
public class Person {

    @Size(max = 3)
    private Property name;

    @Size(min = 50, max = 110, message = "Age can not be less than {min}")
    private Property age;

    @Size(min = -1)
    private Property address;

    @Size
    private Property debt;

    private Property favBook;

    @Size(min = 5)
    private Property friends;

    public void setName(String name) {
        this.name = new Property(name);
    }

    public void setAge(int age) {
        this.age = new Property(age);
    }

    public void setAddress(String address) {
        this.address = new Property(address);
    }

    public void setDebt(double debt) {
        this.debt = new Property(debt);
    }

    public void setFavBook(String favBook) {
        this.favBook = new Property(favBook);
    }

    public <T> void setFriends(List<T> friends) {
        this.friends = new Property(friends);
    }
}

